var args = require("minimist")(process.argv.slice(2));
var VERSION = args.version || require("../package.json").version;

module.exports = {
	banner:
	"/*!\n" +
	" * SNC\n" +
	" * v" + VERSION + "\n" +
	" */\n",
	projectName: "snc",
	paths      : "src/app/**",
	components : "src/app/components",
	utils      : "src/app/scss",
	outputDir  : "snc/"
};
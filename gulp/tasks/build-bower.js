// File Imports
var config = require("../config");
var util   = require("../util");

// NPM Imports
var gulp       = require("gulp");
var gutil      = require("gulp-util");
var series     = require("stream-series");
var bowerFiles = require("gulp-main-bower-files");

// Local Vars
var args = util.args;

// gulp build-bower
exports.task = function() {
	var streams = [],
	dest        = config.outputDir,
	bowerPipe   = undefined;

	gutil.log("Building bower files...");

	streams.push(
		bowerPipe = gulp.src("./bower.json")
		.pipe(bowerFiles())
		.pipe(gulp.dest(dest + "/js/bower"))
		);

	return series(streams);
};
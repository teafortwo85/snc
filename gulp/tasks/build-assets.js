// File Imports
var config = require("../config");

// NPM Imports
var gulp   = require("gulp");
var gutil  = require("gulp-util");
var series = require("stream-series");

// gulp build-assets
exports.task = function() {
	var streams = [],
	dest        = config.outputDir,
	assetPipe   = undefined;

	gutil.log("Building assets...");

	// fonts
	streams.push(
		assetPipe = gulp.src("src/fonts/**/*")
		.pipe(gulp.dest(dest + "/fonts"))
		);

	// 3rd party css
	streams.push(
		assetPipe = gulp.src("src/css/**/*.css")
		.pipe(gulp.dest(dest + "/css"))
		);

	// 3rd party js
	streams.push(
		assetPipe = gulp.src("src/js/**/*.js")
		.pipe(gulp.dest(dest + "/js"))
		);

	return series(streams);
};
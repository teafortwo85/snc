// File Imports
var config = require("../config");
var util   = require("../util");

// NPM Imports
var gulp   = require("gulp");
var concat = require("gulp-concat");
var filter = require("gulp-filter");
var gutil  = require("gulp-util");
var insert = require("gulp-insert");
var jshint = require("gulp-jshint");
var path   = require("path");
var rename = require("gulp-rename");
var series = require("stream-series");
var uglify = require("gulp-uglify");

// Local Vars
var args = util.args;

// gulp build-js
exports.task = function() {
	var streams = [],
	dest        = config.outputDir,
	jsPipe      = undefined;

	gutil.log("Building js files...");

	// templates
	streams.push(
		jsPipe = gulp.src("src/app/components/**/*-template.html")
		.pipe(gulp.dest(dest + "/component"))
		);

	// sections
	streams.push(
		jsPipe = gulp.src("src/app/newcomponents/**/*-section.html")
		.pipe(gulp.dest(dest + "/component"))
		);

	// examples
	streams.push(
		jsPipe = gulp.src("src/app/components/**/*-example.html")
		.pipe(gulp.dest(dest + "/component"))
		);

	// js
	streams.push(
		jsPipe = gulp.src("src/app/**/*.js")
		.pipe(concat(config.projectName + "-library.js"))
		.pipe(gulp.dest(dest + "/js"))
		// .pipe(uglify())
		// .pipe(rename({extname: ".min.js"}))
		// .pipe(gulp.dest(dest + "/js"))
		);

	return series(streams);
};
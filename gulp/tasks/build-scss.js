// File Imports
var config = require("../config");
var util   = require("../util");

// NPM Imports
var autoprefixer = require("gulp-autoprefixer");
var glob         = require("glob");
var gulp         = require("gulp");
var concat       = require("gulp-concat");
var filter       = require("gulp-filter");
var gap          = require("gulp-append-prepend");
var gutil        = require("gulp-util");
var insert       = require("gulp-insert");
var minifyCss    = require("gulp-minify-css");
var path         = require("path");
var rename       = require("gulp-rename");
var sass         = require("gulp-sass");
var series       = require("stream-series");

// Local Vars
var args = util.args;

// gulp build-scss
exports.task = function() {
	var streams = [],
	dest        = config.outputDir,
	basePipe    = undefined,
	scssPipe    = undefined,
	utilsList   = undefined;

	gutil.log("Building css files...");

	streams.push(
		scssPipe = gulp.src(path.join(config.utils, "/**/*.scss"))
		.pipe(util.filterNonCodeFiles())
		.pipe(filter(["**", "!**/base/**.scss"]))
		.pipe(gulp.dest(dest + "/component/_base"))
		);

	setTimeout(function(){
		streams.push(
			basePipe = gulp.src(path.join(config.utils, "/base/**/*.scss"))
			.pipe(util.filterNonCodeFiles())
			.pipe(gulp.dest(dest + "/component/base"))
			.pipe(sass({outputStyle  : "expanded"}))
			.pipe(autoprefixer("last 2 version"))
			.pipe(gulp.dest(dest + "/css"))
			);

		setTimeout(function(){
			streams.push(
				scssPipe = gulp.src(path.join(config.components, "/**/*.scss"))
				.pipe(util.filterNonCodeFiles())
				.pipe(gulp.dest(dest + "/component"))
				.pipe(sass({outputStyle  : "expanded"}))
				.pipe(autoprefixer("last 2 version"))
				.pipe(gulp.dest(dest + "/component"))
				);

			return series(streams);
		}, 500);
	}, 500);
};
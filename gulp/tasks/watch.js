var gulp = require("gulp");

exports.dependencies = ["build"];

exports.task = function() {
	gulp.watch(["src/**/*.scss",
		"src/**/*.js",
		"src/**/*.json",
		"src/**/*.html"],
		["build"]);
};
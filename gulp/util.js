// File Imports
var config = require("./config");

// NPM Imports
var args   = require("minimist")(process.argv.slice(2));
var filter = require("gulp-filter");
var fs     = require("fs");
var gulp   = require("gulp");
var gutil  = require("gulp-util");

exports.args = args;
exports.filterNonCodeFiles = filterNonCodeFiles;

function filterNonCodeFiles() {
	return filter(function(file) {
		return !/demo|module\.json|script\.js|\.spec.js|README/.test(file.path);
	});
}
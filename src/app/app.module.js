(function () {
	var snc = angular.module("sncApp", ["ngRoute"]);
	var $routeProviderReference;
	var currentRoute;
	var resolveRoute;

	// App Configuration
	snc
	.config(function($routeProvider, $locationProvider, $compileProvider) {
		$routeProviderReference = $routeProvider;
		$compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|javascript):/);
	})
	.constant("URL", {
		"base"     : "snc/",
		"js"       : "snc/js/",
		"template" : "snc/component/",
		"img"      : "snc/images/" });

	// Components
	snc.controller("mainCtrl", mainController)
	.controller("shadowCtrl", shadowController);

	snc.run(["$route", "URL", function ($route, URL) {
		$routeProviderReference.when("/", {
			templateUrl: URL.template + "input/input-template.html"
		}).otherwise({redirectTo : "/"});

		$routeProviderReference.when("/input", {
			templateUrl: URL.template + "input/input-template.html"
		}).otherwise({redirectTo : "/"});

		$routeProviderReference.when("/number", {
			templateUrl: URL.template + "number/number-template.html"
		}).otherwise({redirectTo : "/"});

		$routeProviderReference.when("/select", {
			templateUrl: URL.template + "select/select-template.html"
		}).otherwise({redirectTo : "/"});

		$routeProviderReference.when("/select2", {
			templateUrl: URL.template + "select2/select2-template.html"
		}).otherwise({redirectTo : "/"});

		$routeProviderReference.when("/reference", {
			templateUrl: URL.template + "reference/reference-template.html"
		}).otherwise({redirectTo : "/"});

		$routeProviderReference.when("/textarea", {
			templateUrl: URL.template + "textarea/textarea-template.html"
		}).otherwise({redirectTo : "/"});

		$routeProviderReference.when("/checkbox", {
			templateUrl: URL.template + "checkbox/checkbox-template.html"
		}).otherwise({redirectTo : "/"});

		$routeProviderReference.when("/radio", {
			templateUrl: URL.template + "radio/radio-template.html"
		}).otherwise({redirectTo : "/"});

		$routeProviderReference.when("/password", {
			templateUrl: URL.template + "password/password-template.html"
		}).otherwise({redirectTo : "/"});

		$routeProviderReference.when("/phone", {
			templateUrl: URL.template + "phone/phone-template.html"
		}).otherwise({redirectTo : "/"});

		$routeProviderReference.when("/email", {
			templateUrl: URL.template + "email/email-template.html"
		}).otherwise({redirectTo : "/"});

		$routeProviderReference.when("/url", {
			templateUrl: URL.template + "url/url-template.html"
		}).otherwise({redirectTo : "/"});

		$route.reload();
	}]);
})();

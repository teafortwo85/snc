function mainController($scope, $route) {
	$scope.direction = "ltr";

	$scope.toggleDir = function(event){
		if($scope.direction == "ltr"){
			$scope.direction = "rtl";
		} else {
			$scope.direction = "ltr";
		}
	}

	$scope.toggleCompact = function(event){
		$("html").toggleClass("is-compact");
	}
}

function shadowController($scope, $route) {
	setTimeout(function() {
		var htmlArray     = [];
		var jElement      = $(".snc-app");

		if($route.current.$$route.originalPath != "/") {
			var cssFile = "snc/component" + $route.current.$$route.originalPath + $route.current.$$route.originalPath + ".css";
		} else {
			var cssFile = "snc/component/input/input.css";
		}

		jElement.find(".sn-control").each(function(i) {
			var element     = this.outerHTML;
			var marginClass = "";

			if($(this).attr("id")) {
				marginClass = $(this).attr("id");
			}

			$(this).replaceWith(function() {
				return $("<div id='holder-" + i + "' class='snc-shadow-holder " + marginClass +"' />");
			});

			var childRoot = document.getElementById("holder-" + i).attachShadow({mode: 'open'});
			var html = '<style>@import "' + cssFile + '";</style>';
			html += element;

			childRoot.innerHTML = html;
		});

		jElement.addClass("is-loaded");
	}, 500);
}
